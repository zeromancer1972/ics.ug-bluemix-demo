package com.notesx;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "navigator")
@ViewScoped
public class Navigator implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Page> items;
	private String year;

	public Navigator() {
		items = new ArrayList<Page>();
		items.add(new Page("Home", "index.xhtml", "", false));
		items.add(new Page("About", "about.xhtml", "", false));

		Date now = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY");
		year = formatter.format(now);
	}

	public ArrayList<Page> getItems() {
		return items;
	}

	public String active(String key) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String viewId = facesContext.getViewRoot().getViewId();
		return viewId.contains(key) ? "active" : "";
	}

	public String getYear() {
		return year;
	}

}
