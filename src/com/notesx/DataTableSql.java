package com.notesx;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.Context;
import javax.naming.InitialContext;

@ManagedBean(name = "namesSql")
@ViewScoped
public class DataTableSql implements Serializable {

	private static final long serialVersionUID = 1L;

	public DataTableSql() {

	}

	public List<Name> getResult() throws SQLException {
		List<Name> list = new ArrayList<Name>();
		Connection con = null;

		try {

			DriverManager.registerDriver((Driver) Class.forName(
					"org.postgresql.Driver").newInstance());

			String url = "jdbc:postgresql://notesx.net/postgres";
			con = DriverManager.getConnection(url, "johndoe", "password");

			PreparedStatement ps = con
					.prepareStatement("SELECT * FROM xpagesdemo.names ORDER BY lastname");

			// get customer data from database
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Name n = new Name();
				n.setFirstname(result.getString("firstname"));
				n.setLastname(result.getString("lastname"));
				n.setId(result.getInt("id"));
				// store all data into a List
				list.add(n);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (Exception e2) {
			}
		}

		return list;
	}

}
