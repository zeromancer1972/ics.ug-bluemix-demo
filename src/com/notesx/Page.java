package com.notesx;

public class Page {

	private String label;
	private String url;
	private String icon;
	private boolean newWindow;

	public Page() {

	}

	public Page(String label, String url, String icon, boolean newWindow) {
		this.label = label;
		this.url = url;
		this.icon = icon;
		this.newWindow = newWindow;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public boolean isNewWindow() {
		return newWindow;
	}

	public void setNewWindow(boolean newWindow) {
		this.newWindow = newWindow;
	}

}
