package com.notesx;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

@ManagedBean (name="page")
public class PageController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String active(String key) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String viewId = facesContext.getViewRoot().getViewId();
		return viewId.indexOf(key) != -1 ? "active" : "";
	}

}
