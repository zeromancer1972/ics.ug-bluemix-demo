package com.notesx;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "names")
public class DataTable implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Name> data;

	public DataTable() {
		init();
	}

	public void init() {
		data = new ArrayList<Name>();
		data.add(new Name("Busse", "Oliver", 0));
		data.add(new Name("Doe", "John", 1));
		data.add(new Name("Bourne", "Mel", 2));
		data.add(new Name("Burg", "Johannes", 3));
	}

	public ArrayList<Name> getData() {
		return data;
	}

	public void setData(ArrayList<Name> data) {
		this.data = data;
	}

}
