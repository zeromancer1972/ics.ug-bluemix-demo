# JSF Bluemix Demo #

This is the sample app used at ICS.UG and ICON.UK in 2015
It accesses a PostgreSQL database on another server, utilizes the Bootstrap UI framework and uses JSF to render pages.
It's not a high level JSF application and is only to for showing the workflow using various tools to develop with Bluemix.
